# - Try to find LibIRCClient
# LibIRCClient_FOUND - system has LibIRCClient
# LibIRCClient_INCLUDE_DIRS - the LibIRCClient include directories
# LibIRCClient_LIBRARIES - link these to use LibIRCClient

# Find Include 
find_path(LibIRCClient_INCLUDE_DIR
	NAMES libircclient.h
	PATHS ${LibIRCClient_PKGCONF_INCLUDE_DIRS}
	/usr/include /sw/include /usr/local/include
	/usr/include/libircclient /sw/include/libircclient
	/usr/local/include/libircclient
	)
# Find Library
find_library(LibIRCClient_LIBRARIES
	NAMES ircclient
	PATHS ${LibIRCClient_PKGCONF_LIBRARY_DIRS}
	/usr/lib /lib /sw/lib /usr/local/lib
	)
#Print message
if(LibIRCClient_LIBRARIES AND LibIRCClient_INCLUDE_DIR)
	set(LibIRCClient_FOUND 1)
	message(STATUS "Found LibIRCClient at ${LibIRCClient_LIBRARIES}")
else()
	message(FAILURE "Could not find LibIRCClient ") 
endif(LibIRCClient_LIBRARIES AND LibIRCClient_INCLUDE_DIR)

#TODO: Do i need this?
set(LibIRCClient_PROCESS_INCLUDES LibIRCClient_INCLUDE_DIR)
set(LibIRCClient_PROCESS_LIBS LibIRCClient_LIBRARIES)

