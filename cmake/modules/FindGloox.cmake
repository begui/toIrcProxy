# - Try to find GLOOX
# GLOOX_FOUND True if gloox got found
# GLOOX_INCLUDE_DIR Location of gloox headers
# GLOOX_LIBRARIES List of libaries to use gloox

# Find Include 
find_path( GLOOX_INCLUDE_DIR 
	NAMES gloox.h 
	PATHS ${GLOOX_PKGCONF_INCLUDE_DIRS}
	/usr/include /usr/local/include
	/usr/include/gloox /usr/local/include/gloox
	)
# Find Library
find_library( GLOOX_LIBRARIES 
	NAMES gloox 
	PATHS ${GLOOX_PKGCONF_LIBRARY_DIRS}
	/usr/lib /usr/local/lib
	)
#Print message
if( GLOOX_LIBRARIES AND GLOOX_INCLUDE_DIR )
	set( GLOOX_FOUND 1 )
	message( STATUS "Found Gloox at ${GLOOX_LIBRARIES}" )
else( GLOOX_LIBRARIES AND GLOOX_INCLUDE_DIR )
	message( STATUS "Could NOT find gloox" )
endif( GLOOX_LIBRARIES AND GLOOX_INCLUDE_DIR )
