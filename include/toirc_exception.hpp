#pragma once

class ToIrcException : public std::runtime_error {
 public:
  explicit ToIrcException ( const char *message ) : std::runtime_error ( std::string ( message ) ) {}
  explicit ToIrcException ( const std::string &message ) : std::runtime_error ( message ) {}
  virtual ~ToIrcException ( ) throw ( ) {}
};
