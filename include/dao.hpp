#pragma once

#include <map>
#include <memory>
#include <string>
#include <vector>

#include <sqlite_modern_cpp.h>

constexpr auto default_dbname{"toirc.db"};

enum class ENV_VARS_TYPE {
  FILE_DB,  //
  FILE_LOG,
  JABBER_USER,
  JABBER_PASS,
  IRC_HOST,
  IRC_PORT,
  POLL_INTERVAL
};

class Env {
 public:
  /**
   * @brief returns true if env exists
   */
  static bool exist ( ENV_VARS_TYPE type ) noexcept;
  /**
   * @brief gets the environment variables value.
   * @return null if it DNE
   */
  static const char *get ( ENV_VARS_TYPE type ) noexcept;
  static const char *get ( ENV_VARS_TYPE type, const char *defaultVal ) noexcept;
  static const std::string get ( ENV_VARS_TYPE type, const std::string &defaultVal ) noexcept;
  /**
   * @brief gets the environment variable value
   * @return default value if it DNE
   */
  static std::int32_t get ( ENV_VARS_TYPE type, std::int32_t defaultVal ) noexcept;

 public:
  static std::map< ENV_VARS_TYPE, std::string > ENV_VARS;
};
/**
 *
 */
struct User {
  enum class USER_ROLE : int { ADMIN = 1, USER };
  User ( ) = default;
  User ( std::string xmppuser, std::string ircuser, std::string ircpass, bool listen, USER_ROLE role )
      : xmppuser ( std::move ( xmppuser ) ),
        ircuser ( std::move ( ircuser ) ),
        ircpass ( std::move ( ircpass ) ),
        listen ( listen ),
        role ( role ) {}
  std::string xmppuser;
  std::string ircuser;
  std::string ircpass;
  bool listen;
  User::USER_ROLE role{User::USER_ROLE::USER};
};

namespace db {
/**
 * @brief Initializes the database file
 */
void init ( const std::string &dbname );

std::string select_lookup_value ( const std::string &dbName, const std::string &key );
/**
 * @brief Inserts a new record into the lookup table
 */
void insert_lookup ( const std::string &dbname, const std::string &key, const std::string &value );
/**
 * @brief Checks to see if the key exists in the database
 */
bool check_lookup ( const std::string &dbname, const std::string &key );
/**
 * @brief inserts a new xmpp/ircuser
 */
void insert_irc_user ( const std::string &dbname, const std::string &xmppuser, const std::string &ircuser,
                       const std::string &ircpass );
/**
 * @return a bool indicating if the irc user exists.
 */
bool check_irc_user ( const std::string &dbname, const std::string &ircuser );
/**
 * @brief updates a users listening state.
 */
void update_listen ( const std::string &dbname, const std::string &xmppuser, bool listen );
/**
 * @return a list of users
 */
std::vector< User > select_users ( const std::string &dbname );
/**
 * @return an individual user
 */
std::unique_ptr< User > select_user ( const std::string &dbName, const std::string &xmppuser );
}  // end of namespace db

namespace validation {

bool check ( ENV_VARS_TYPE type );

}  // end of namespace validation

namespace dao {

std::string getJabberUser ( );
std::string getJabberPass ( );
std::string getIrcHost ( );
std::int32_t getIrcPort ( );
}  // end of namespace dao
