#pragma once

#include <string>

#include <libirc_rfcnumeric.h>
#include <libircclient.h>

#include <functional>
#include <map>
#include <memory>
#include <unordered_map>

class IrcListener {
 public:
  enum class IRC_EVENT { CONNECT, DISCONNECT, JOINED, LEFT, CHAT, NAMES };

 public:
  virtual void handleIrcMessage ( IRC_EVENT event, const std::string &to, const std::string &from,
                                  const std::string &msg ) = 0;
};

struct irc_ctx_t {
  std::string channel;
  std::string nick;
  std::string passwd;
  IrcListener *listener{nullptr};
};

using SessionPtr = std::unique_ptr< irc_session_t, std::function< void( irc_session_t *session ) > >;

class IrcSession final {
 public:
  IrcSession ( const irc_ctx_t & );
  // Copy Constructor
  IrcSession ( const IrcSession & ) = delete;
  // Copy Assignment Operator 
  IrcSession operator= ( IrcSession & ) = delete;
  // Move Constructor
  IrcSession ( IrcSession && ) = default;
  // Move Assignment Operator
  IrcSession &operator= ( IrcSession && ) = delete;
  // Destructor 
  ~IrcSession() = default;
  // 
  operator irc_session_t * ( ) const;
  
 public:
  /**
   *
   **/
  void getNames ( ) const;
  /**
   *
   */
  bool isConnected ( ) const;
  /**
   *
   */
  void connect ( );
  /**
   *
   */
  void send ( const std::string &msg ) const;
  /**
   *
   **/
  void recv ( int microseconds = -1 ) const;

 private:
  SessionPtr session_{nullptr};
  irc_callbacks_t callbacks_;
  irc_ctx_t ctx_;
};
