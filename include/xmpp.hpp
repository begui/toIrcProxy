#pragma once

#include <gloox/client.h>
#include <gloox/connectionlistener.h>
#include <gloox/connectiontcpclient.h>
#include <gloox/message.h>
#include <gloox/messagehandler.h>
#include <memory>

class XmppListner {
 public:
  virtual void handleXmppMessage ( const std::string &from, const std::string &fromfull, const std::string &msg ) = 0;
};

class XmppSession : public gloox::ConnectionListener, gloox::MessageHandler {
 public:
  XmppSession ( const char *user, const char *pass );
  XmppSession ( const std::string &user, const std::string &pass );
  // Copy Constructor
  XmppSession ( const XmppSession & ) = delete;
  // Copy Assignment Operator
  XmppSession operator= ( XmppSession & ) = delete;
  // Move Constructor
  XmppSession ( XmppSession && ) = default;
  // Move Assignment Operator
  XmppSession &operator= ( XmppSession && ) = delete;
  // Destructor
  virtual ~XmppSession ( );

 public:
  /**
   *
   */
  bool shouldIConnect ( ) const { return connect_; }
  /**
   *
   */
  void registerListener ( XmppListner *listener );
  /**
   *
   */
  void connect ( bool blocking = false );
  /**
   *
   */
  void send ( const std::string &from, const std::string &msg ) const;
  /**
   *
   */
  void recv ( int microseconds = -1 );

 protected:
  void onConnect ( ) noexcept override;
  void onDisconnect ( gloox::ConnectionError e ) noexcept override;
  bool onTLSConnect ( const gloox::CertInfo &info ) noexcept override;
  void handleMessage ( const gloox::Message &stanza, gloox::MessageSession *session = nullptr ) noexcept override;

 private:
  XmppListner *listener_{nullptr};
  std::unique_ptr< gloox::Client > client_{nullptr};
  bool blocking_{false};
  bool connect_{true};
  int sockfd_{-1};
  std::string user_;
  std::string pass_;
};
