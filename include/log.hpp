#pragma once

#include <spdlog/spdlog.h>

template < typename T >
void UNUSED ( T && ) {}

namespace logger {
enum class LOGTYPE { CONSOLE_WRITER, FILE_WRITER };
using LogShrdPtr = std::shared_ptr< spdlog::logger >;
extern LogShrdPtr &Log ( );

/**
 *
 */
void init ( LOGTYPE logtype, std::string appname, std::string filename = "applog", int sizeInMeg = 5, int max = 3 );
} // end of namespace logger
