#pragma once

#include <ncurses.h>
#include <iostream>
#include <memory>

struct Deleter {
 public:
  void operator( ) ( WINDOW* window ) {
    if ( window ) delwin ( window );
  }
};
using WindowUniqPtr = std::unique_ptr< WINDOW, Deleter >;

class Window final {
 public:
  Window ( int rows = 0, int columns = 0);

  operator WINDOW * (){
    return window_.get();
  }

 public:
  void refresh ( ); 
  void highlight (bool enable = true);
  void keypad ( bool enable = true ); 
  void write ( const char* str, int x, int y, bool highlight = false ); 
  void input( const char* label, std::string &input, int x, int y, bool hide ); 
  void box(std::uint32_t vert, std::uint32_t horz);
  void clear();
  void clearLine(int row, int column);
  int getChar();

 private:
  WindowUniqPtr window_{nullptr};
};

class ConfigMenu final {
 public:
  enum Selection {
    BEGIN,  //
    Irc,
    Xmpp,
    Path,
    END
  };

 public:
  ConfigMenu ( );
  ~ConfigMenu ( );

 public:
  void next ( );
  void prev ( );
  void run ( );

 public:
  /**
   * Prevents writing output to string
   */
  void echo ( bool enable );

 public:
  ConfigMenu::Selection getCurrentMenuItem ( ) { return currentMenuItem_; }

 private:
  ConfigMenu::Selection currentMenuItem_{ConfigMenu::Selection::Irc};
};

