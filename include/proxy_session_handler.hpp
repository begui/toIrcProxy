#pragma once

#include <functional>
#include <unordered_map>
#include <vector>

#include "dao.hpp"
#include "irc.hpp"
#include "xmpp.hpp"

class ProxySessionHandler final : public XmppListner, IrcListener {
 public:
  ProxySessionHandler ( XmppSession &&session, const std::string &file, const std::string &channel );
  // Copy Constructor
  ProxySessionHandler ( const ProxySessionHandler & ) = delete;
  // Copy Assignment Operator 
  ProxySessionHandler operator= ( ProxySessionHandler & ) = delete;
  // Move Constructor
  ProxySessionHandler ( ProxySessionHandler && ) = delete;
  // Move Assignment Operator
  ProxySessionHandler &operator= ( ProxySessionHandler && ) = delete;
  // Destructor
  ~ProxySessionHandler() = default;
  /**
   *
   */
  void run ( int timeout );
  /**
   *
   */
  void handleXmppMessage ( const std::string &from, const std::string &fromfull, const std::string &msg ) override;
  /**
   *
   */
  void handleIrcMessage ( IRC_EVENT event, const std::string &to, const std::string &from,
                          const std::string &msg ) override;
 private:
  /*
   * Adds a 'connected' member to members list
   *
   **/
  void addMember ( const User &user, bool connect = false );
  /**
   *
   */
  void disconnectIrcSession ( const std::string &from );

 private:
  XmppSession xmppsession_;
  std::string filename_;
  std::string channel_;
  std::vector< User > members_;
  std::unordered_map< std::string, IrcSession > ircSessions_;
  std::unordered_map< std::string, std::function< void( const std::string &from, const std::string &fromfull,
                                                        const std::string &msg ) > >
      commandHandlers_;
};
