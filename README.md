xmppToIRC
==========

This software allows you to proxy messages from a client that supports xmpp (hangouts) to a user of an IRC channel 
![toIrcFlow.png](doc/toIrcFlow.png)

#Dependencies

```

sudo apt-get install libgloox-dev libircclient-dev libsqlite3-dev libncurses-dev
git submodule update --init --recursive
```

#Required

```
#!bash

export JABBER_USER=realgamedev.chatter@gmail.com
export JABBER_PASS=###
export IRC_HOST=irc.freenode.net
export IRC_PORT=8001
```
#Note on the proxy gmail account

The gmail account that the proxy uses needs to have the option to allow less secure apps to log into the account (this is because the proxy is using Jabber). This is located in _My Account > Sign-in & Security_ and the option is titled __Allow less secure apps__. Make sure this option is set to __ON__.

Example usage 
-------------

Add the user to your gchat friends. 

If you are having issues logging into Googles Jabber server, you need to register it's cookie to your box. One way I was able to accomplish this was to login my google account via Lynx. 

#Commands

A list of commands to call 

* !help
* !register {user} {password}
* !on
* !off
* !who 

Known Problems
--------------
* Sometimes fails to reconnect netsplits. Needs to notify the user of potential issues
* Upgrading to latest ubuntu server 16.04 comes with the gloox 1.0.13 which contains a bug. see workaround
  - http://stackoverflow.com/questions/34919544/sigsegv-simple-gloox-client
  - Download the 1.0.17 version of gloox, build, and install
  - You may need to set the LD_LIBRARY_PATH to point to /usr/local/lib

TODOs
-----
* Encrypt passwords or database.
* Set syslog logging to default. Allow user to specify path/filename.
* Add way to set Admin user via ConfigMenu
* Look into glooxd as a mock server for unit tests
