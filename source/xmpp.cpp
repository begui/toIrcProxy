#include "xmpp.hpp"
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <ostream>
#include "log.hpp"

std::ostream& operator<< ( std::ostream& os, gloox::Message::MessageType type ) {
  switch ( type ) {
    case gloox::Message::Chat:
      os << "Chat";
      break;
    case gloox::Message::Error:
      os << "Error";
      break;
    case gloox::Message::Groupchat:
      os << "Groupchat";
      break;
    case gloox::Message::Headline:
      os << "Headline";
      break;
    case gloox::Message::Normal:
      os << "Normal";
      break;
    case gloox::Message::Invalid:
      os << "Invalid";
      break;
    default:
      os << "unknown type";
      break;
  }
  return os;
}

std::ostream& operator<< ( std::ostream& os, const gloox::Message& stanza ) {
  os << "TYPE: " << stanza.subtype ( ) << "\nFROM: " << stanza.from ( ).full ( )
     << "\nSFRO: " << stanza.from ( ).username ( ) << "\nBODY: " << stanza.body ( ) << "\nSUBJ: " << stanza.subject ( );
  return os;
}

XmppSession::XmppSession ( const char* user, const char* pass ) : user_ ( user ), pass_ ( pass ) {}

XmppSession::XmppSession ( const std::string& user, const std::string& pass ) : user_ ( user ), pass_ ( pass ) {}
XmppSession::~XmppSession ( ) {
  if ( client_ ) {
    client_->disconnect ( );
  }
}
void XmppSession::registerListener ( XmppListner* listener ) { listener_ = listener; }

void XmppSession::onConnect ( ) noexcept {
  logger::Log ( )->trace ( "onConnect" );
  sockfd_ = static_cast< gloox::ConnectionTCPClient* > ( client_->connectionImpl ( ) )->socket ( );
}

void XmppSession::onDisconnect ( gloox::ConnectionError e ) noexcept {
  logger::Log ( )->trace ( "onDisconnect" );

  switch ( e ) {
    case gloox::ConnNoError:
      break;
    case gloox::ConnStreamError:
      logger::Log ( )->error ( "stream error" );
      break;
    case gloox::ConnStreamVersionError:
      logger::Log ( )->error ( "incoming stream version not supported" );
      break;
    case gloox::ConnStreamClosed:
      logger::Log ( )->error ( "connection closed by the server" );
      break;
    case gloox::ConnProxyAuthRequired:
    case gloox::ConnProxyAuthFailed:
    case gloox::ConnProxyNoSupportedAuth:
      logger::Log ( )->error ( "proxy authentication failed" );
      break;
    case gloox::ConnIoError:
      logger::Log ( )->error ( "connection I/O error" );
      break;
    case gloox::ConnParseError:
      logger::Log ( )->error ( "XML parse error" );
      break;
    case gloox::ConnConnectionRefused:
      logger::Log ( )->error ( "server refused connection" );
      break;
    case gloox::ConnDnsError:
      logger::Log ( )->error ( "could not resolve server hostname" );
      break;
    case gloox::ConnOutOfMemory:
      logger::Log ( )->error ( "out of memory" );
      break;
    case gloox::ConnNoSupportedAuth:
      logger::Log ( )->error ( "no supported authentication mechanism" );
      break;
    case gloox::ConnTlsFailed:
      logger::Log ( )->error ( "TLS veification failed" );
      break;
    case gloox::ConnTlsNotAvailable:
      logger::Log ( )->error ( "TLS not available" );
      break;
    case gloox::ConnCompressionFailed:
      logger::Log ( )->error ( "compression error" );
      break;
    case gloox::ConnAuthenticationFailed:
      logger::Log ( )->error ( "authentication failed " );
      break;
    case gloox::ConnUserDisconnected:
      break;
    case gloox::ConnNotConnected:
      break;
    default:
      logger::Log ( )->error ( "Unknown error" );
      break;
  }

  if ( -1 != sockfd_ ) {
    logger::Log ( )->debug ( "Closing sockfd {}", sockfd_ );
    close ( sockfd_ );
    sockfd_ = -1;
  }
  connect_ = true;
}

bool XmppSession::onTLSConnect ( const gloox::CertInfo& ) noexcept {
  // Assume things are valid for now
  return true;
}

void XmppSession::handleMessage ( const gloox::Message& stanza, gloox::MessageSession* ) noexcept {
  logger::Log ( )->info ( "Message from {}", stanza.from ( ) );
  if ( stanza.body ( ).length ( ) > 0 ) {
    if ( listener_ ) {
      auto pos = stanza.from ( ).full ( ).find ( "/" );
      std::string name = stanza.from ( ).full ( ).substr ( 0, pos );

      listener_->handleXmppMessage ( name, stanza.from ( ).full ( ), stanza.body ( ) );
    }
  }
}

void XmppSession::connect ( bool blocking ) {
  connect_ = false;
  logger::Log ( )->info( "Connecting to XMPP server" );
  blocking_ = blocking;

  gloox::JID jid ( user_ );
  client_ = std::make_unique< gloox::Client > ( jid, pass_ );

  if ( client_ ) {
    client_->registerMessageHandler ( this );
    client_->registerConnectionListener ( this );
    client_->setStreamManagement ( true, true );

    if ( client_->connect ( blocking_ ) ) {
      gloox::ConnectionError ce = gloox::ConnNoError;
      while ( ce == gloox::ConnNoError && sockfd_ < 0 ) {
        ce = client_->recv ( );
      }
    }
  }
}

void XmppSession::send ( const std::string& from, const std::string& msg ) const {
  logger::Log ( )->debug ( "Sending xmpp Message {} from {}", msg, from );
  client_->send ( {gloox::Message::Chat, from, msg} );
}

void XmppSession::recv ( int microseconds ) {
  gloox::ConnectionError ce = client_->recv ( microseconds );
  if ( ce != gloox::ConnNoError ) {
    if ( -1 != sockfd_ ) {
      logger::Log ( )->warn ( "Closing sockfd {}", sockfd_ );
      close ( sockfd_ );
      sockfd_ = -1;
    }
    connect_ = true;
    logger::Log ( )->error ( "Gloox error: {}", ce );
  }
}
