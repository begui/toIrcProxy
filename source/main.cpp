#include <getopt.h>
#include <sys/resource.h>
#include <algorithm>
#include <atomic>
#include <csignal>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <iostream>
#include <memory>

#include "config.hpp"
#include "dao.hpp"
#include "toirc_exception.hpp"
#include "toirc_proxy.hpp"
#include "version.hpp"

constexpr int poll_interval_microsec{100000};

std::atomic< bool > g_quit ( false );

/**
 * @brief The point of No return.
 **/
inline void quit ( const std::string &error ) {
  g_quit = true;
  if ( !error.empty ( ) ) {
    logger::Log ( )->error ( "Quitting , {}", error );
  }
}
[[noreturn]] inline void exit ( const std::string &error ) {
  if ( !error.empty ( ) ) {
    logger::Log ( )->error ( "Exiting, {}", error );
  }
  std::exit ( -1 );
}

/**
 * @brief binds lambda function to some signal types
 */
void enable_signal ( ) {
  auto SignalHandler = []( int sig ) {
    switch ( sig ) {
      case SIGHUP:
        exit ( "HANGUP" );
        break;
      case SIGINT:
      case SIGTERM:
        quit ( "Ctl-c Detected" );
        break;
      case SIGSEGV:
        exit ( "Segmentation violation" );
        break;
      case SIGQUIT:
        quit ( "Quit" );
        break;
        // case SIGKILL:
        //  exit ( "Kill" );
        break;
      default:
        exit ( "Unknown signal" );
        break;
    }
  };
  std::signal ( SIGHUP, SignalHandler );
  std::signal ( SIGINT, SignalHandler );
  std::signal ( SIGSEGV, SignalHandler );
  std::signal ( SIGTERM, SignalHandler );
  std::signal ( SIGQUIT, SignalHandler );
  // std::signal ( SIGKILL, SignalHandler );

  std::set_terminate ( [] {
    try {
      throw;
    } catch ( const std::exception &e ) {
      exit ( e.what ( ) );
    } catch ( const char *p ) {
      exit ( p );
    } catch ( ... ) {
      exit ( "Unknown exception" );
    }
  } );
  std::set_unexpected ( [] {
    try {
      throw;
    } catch ( const std::exception &e ) {
      exit ( e.what ( ) );
    } catch ( const char *p ) {
      exit ( p );
    } catch ( ... ) {
      exit ( "Unknown exception" );
    }
  } );
}

/**
 * @brief Produces core dump
 */
void enable_coredump ( ) {
  struct rlimit core_limits;
  core_limits.rlim_cur = core_limits.rlim_max = RLIM_INFINITY;
  setrlimit ( RLIMIT_CORE, &core_limits );
}

/**
 * @brief Runs the Ncurses Config menu if required variables are missing
 */
void data_validation_check ( bool runMenu ) {
  bool isJabberUserDefined = validation::check ( ENV_VARS_TYPE::JABBER_USER );
  bool isJabberUserPassDefined = validation::check ( ENV_VARS_TYPE::JABBER_PASS );
  bool isIrcHostDefined = validation::check ( ENV_VARS_TYPE::IRC_HOST );
  bool isIrcPortDefined = validation::check ( ENV_VARS_TYPE::IRC_PORT );

  bool flag = isJabberUserDefined && isJabberUserPassDefined && isIrcHostDefined && isIrcPortDefined;

  if ( !flag || runMenu ) {
    ConfigMenu menu;
    menu.run ( );
  }
}

/**
 * Start of main
 */
int main ( int argc, char *argv[] ) {
  // TODO: set default logging to console, require path/filename
  //
  // Program options
  //
  std::string channel;
  bool consoleLog{false};
  bool runMenu{false};

  const char *const shortOptions = "c:l:vm";
  const struct option longOptions[] = {{"channel", 1, NULL, 'c'},  //
                                       {"consoleLog", 1, NULL, 'l'},
                                       {"version", 0, NULL, 'v'},
                                       {"menu", 0, NULL, 'm'},
                                       {NULL, 0, NULL, 0}};
  //
  // Parse options
  //
  while ( true ) {
    int optionNext = getopt_long ( argc, argv, shortOptions, longOptions, NULL );
    if ( optionNext == -1 ) {
      break;
    }
    switch ( optionNext ) {
      case 'c':  // -c or --channel
        channel = optarg;
        break;
      case 'l':
        if ( strcmp ( optarg, "true" ) == 0 || atoi ( optarg ) == 1 ) {
          consoleLog = true;
        }
        break;
      case 'v':
        std::cout << TOIRC_VERSION_MAJOR << "." << TOIRC_VERSION_MINOR << "." << TOIRC_VERSION_PATCH << std::endl;
        return EXIT_SUCCESS;
        break;
      case 'm':
        runMenu = true;
        break;
      case '?':  // Invalid option
      case -1:   // No more options
        break;
      default:  // Something unexpected? Aborting
        return ( 1 );
    }  // end of switch
  }    // end of while

  if ( consoleLog ) {
    logger::init ( logger::LOGTYPE::CONSOLE_WRITER, argv[ 0 ] );
  } else {
    logger::init ( logger::LOGTYPE::FILE_WRITER, argv[ 0 ] );
  }
  logger::Log ( )->set_level ( spdlog::level::info );

  if ( channel.length ( ) == 0 ) {
    exit ( "Missing channel parameter (-c channelName)" );
  }
  if ( channel[ 0 ] != '#' ) {
    channel.insert ( 0, "#" );
  }

  //
  // Verify we have all the environment variables defined
  //
  try {
    db::init ( Env::get ( ENV_VARS_TYPE::FILE_DB, default_dbname ) );
    data_validation_check ( runMenu );
    enable_signal ( );
    enable_coredump ( );
    std::set_new_handler ( [] { exit ( "Memory Allocation Failed." ); } );

    bool flags = validation::check ( ENV_VARS_TYPE::JABBER_USER )     //
                 && validation::check ( ENV_VARS_TYPE::JABBER_PASS )  //
                 && validation::check ( ENV_VARS_TYPE::IRC_HOST )     //
                 && validation::check ( ENV_VARS_TYPE::IRC_PORT );
    if ( !flags ) {
      throw ToIrcException ( "Missing one of the configuration values, Please run the config menu" );
    }

    ProxySessionHandler proxy (                               //
        {dao::getJabberUser ( ), dao::getJabberPass ( )},     //
        Env::get ( ENV_VARS_TYPE::FILE_DB, default_dbname ),  //
        channel );
    proxy.run ( poll_interval_microsec );
  } catch ( ToIrcException &e ) {
    logger::Log ( )->error ( e.what ( ) );
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}
