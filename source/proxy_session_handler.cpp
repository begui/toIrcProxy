#include "proxy_session_handler.hpp"
#include <algorithm>
#include <atomic>
#include <fstream>
#include <iterator>
#include <sstream>
#include "log.hpp"

extern std::atomic< bool > g_quit;

ProxySessionHandler::ProxySessionHandler ( XmppSession &&xmppSession, const std::string &file,
                                           const std::string &channel )
    : xmppsession_ ( std::move ( xmppSession ) ),
      filename_ ( file ),
      channel_ ( channel ),
      commandHandlers_ (
          {{"!register",
            [=]( const std::string &from, const std::string &fromfull, const std::string &msg ) {
              logger::Log ( )->debug ( "From : {}, FromFull: {}, Msg: {}", from, fromfull, msg );
              //
              // Parse message string
              // TODO: implement a better way to parse this
              //
              std::string userCommand;
              std::string ircname;
              std::string ircpasswd;  // may be optional
              std::istringstream iss{msg};
              // iss >> userCommand >> ircname >> ircpasswd;
              std::istream_iterator< std::string > begin ( iss );
              std::istream_iterator< std::string > end;
              std::vector< std::string > msgVector;
              std::copy ( begin, end, std::back_inserter ( msgVector ) );
              if ( msgVector.size ( ) > 1 ) {
                userCommand = msgVector[ 0 ];
                ircname = msgVector[ 1 ];
              }
              if ( msgVector.size ( ) > 2 ) {
                ircpasswd = msgVector[ 2 ];
              }

              logger::Log ( )->debug ( "(UserCommand: {}), (ircname: {}), (ircpasswd: {})", userCommand, ircname,
                                       ircpasswd );
              if ( ircname.length ( ) != 0 ) {
                db::insert_irc_user ( filename_, from, ircname, ircpasswd );
                //
                // Push users into user list
                //
                xmppsession_.send ( fromfull, "You have successfully registered. To login, use the '!on' command." );

              } else {
                xmppsession_.send ( fromfull, "Invalid command arguments" );
                xmppsession_.send ( fromfull, "!register {name} {password - optional}" );
              }
              logger::Log ( )->debug ( "End of register" );
            }},
           {"!on",
            [=]( const std::string &from, const std::string &fromfull, const std::string &msg ) {
              UNUSED ( msg );

              auto memberIter = std::find_if ( members_.begin ( ), members_.end ( ),
                                               [&from]( const User &user ) { return user.xmppuser == from; } );
              if ( memberIter != members_.end ( ) ) {
                memberIter->listen = true;
                //
                // Disconnect from current session
                //
                disconnectIrcSession ( from );
                //
                // Try to connect to IRC session
                //
                ircSessions_.emplace ( from, IrcSession ( {channel, memberIter->ircuser, memberIter->ircpass, this} ) );
                auto it = ircSessions_.find ( from );
                if ( it != ircSessions_.end ( ) ) {
                  it->second.connect ( );
                }
                db::update_listen ( filename_, from, true);
              } else {
                auto user = db::select_user ( filename_, from );
                if ( user ) {
                  addMember ( *user, true );

                  memberIter = std::find_if ( members_.begin ( ), members_.end ( ),
                                              [&from]( const User &user ) { return user.xmppuser == from; } );

                  if ( memberIter != members_.end ( ) ) {
                    ircSessions_.emplace ( from,
                                           IrcSession ( {channel, memberIter->ircuser, memberIter->ircpass, this} ) );
                    auto it = ircSessions_.find ( from );
                    if ( it != ircSessions_.end ( ) ) {
                      it->second.connect ( );
                    }
                  }

                } else {
                  xmppsession_.send ( fromfull, "You are not registered." );
                }
              }
            }},
           {"!off",
            [=]( const std::string &from, const std::string &fromfull, const std::string &msg ) {
              UNUSED ( msg );
              disconnectIrcSession ( from );

              auto memberIter = std::find_if ( members_.begin ( ), members_.end ( ),
                                               [&from]( const User &user ) { return user.xmppuser == from; } );

              if ( memberIter != members_.end ( ) ) {
                memberIter->listen = false;
                db::update_listen ( filename_, from, false );
                xmppsession_.send ( fromfull, "You are no longer listening." );
              }
            }},
           {"!me",
            [=]( const std::string &from, const std::string &fromfull, const std::string &msg ) {
              UNUSED ( from );
              UNUSED ( fromfull );
              UNUSED ( msg );
            }},
           {"!who",
            [=]( const std::string &from, const std::string &fromfull, const std::string &msg ) {
              UNUSED ( fromfull );
              UNUSED ( msg );
              auto session = ircSessions_.find ( from );
              if ( session != ircSessions_.end ( ) ) {
                session->second.getNames ( );
              }

            }},
           {"!help",
            [=]( const std::string &from, const std::string &fromfull, const std::string &msg ) {
              UNUSED ( from );
              UNUSED ( msg );
              std::ostringstream helpMsg;
              helpMsg << "* !register {user-required} {passwd-optional}" << std::endl
                      << "* !on" << std::endl
                      << "* !off" << std::endl
                      << "* !who";

              xmppsession_.send ( fromfull, helpMsg.str ( ) );
            }},
           {"!echo", [=]( const std::string &from, const std::string &fromfull, const std::string &msg ) {
              UNUSED ( from );
              xmppsession_.send ( fromfull, msg );
            }}} ) {
  xmppsession_.registerListener ( this );
}

void ProxySessionHandler::handleIrcMessage ( IRC_EVENT event, const std::string &to, const std::string &from,
                                             const std::string &msg ) {
  logger::Log ( )->info ( "IRC MESSAGE: from {} to {} with message: {}", from, to, msg );

  auto memberIter =
      std::find_if ( members_.begin ( ), members_.end ( ), [&to]( const User &user ) { return user.ircuser == to; } );

  if ( memberIter != members_.end ( ) ) {
    if ( event == IRC_EVENT::JOINED ) {
      xmppsession_.send ( memberIter->xmppuser,
                          ( to == from ) ? "** You have joined! **" : "** " + from + " has joined! **" );
    } else if ( event == IRC_EVENT::LEFT ) {
      xmppsession_.send ( memberIter->xmppuser, "** " + from + " has left! **" );
    } else if ( event == IRC_EVENT::CHAT || event == IRC_EVENT::NAMES ) {
      xmppsession_.send ( memberIter->xmppuser, from + ": " + msg );
    } else {
      // TODO: Unhandled event
    }
  } else {
    logger::Log ( )->warn ( "User {} was not found", to );
  }
}

void ProxySessionHandler::handleXmppMessage ( const std::string &from, const std::string &fromfull,
                                              const std::string &msg ) {
  logger::Log ( )->info ( "XMPP MESSAGE: from {} with message: {}", from, msg );
  if ( msg[ 0 ] != '!' ) {
    auto sessionIter = ircSessions_.find ( from );
    if ( sessionIter != ircSessions_.end ( ) ) {
      sessionIter->second.send ( msg );
    }
  } else {
    //
    //  get the first word in string
    //
    std::istringstream iss{msg};
    std::string userCommand;
    iss >> userCommand;
    //
    // process if command
    //
    auto commandHandlerIter = commandHandlers_.find ( userCommand );
    if ( commandHandlerIter != commandHandlers_.end ( ) ) {
      commandHandlerIter->second ( from, fromfull, msg );
    } else {
      xmppsession_.send ( fromfull, "Invalid command: " + msg );
    }
  }
}

void ProxySessionHandler::addMember ( const User &user, bool connect ) {
  logger::Log ( )->debug ( "setting user " );
  logger::Log ( )->debug ( "setting channel to {}", channel_ );
  logger::Log ( )->debug ( "setting nick to {}", user.ircuser );
  logger::Log ( )->debug ( "setting passwd " );
  logger::Log ( )->debug ( "setting listener" );

  if ( connect ) {
    ircSessions_.emplace ( user.xmppuser, IrcSession ( {channel_, user.ircuser, user.ircpass, this} ) );
    auto it = ircSessions_.find ( user.xmppuser );
    if ( it != ircSessions_.end ( ) ) {
      it->second.connect ( );
    }

  } else {
    logger::Log ( )->error ( "Error finding member {}", user.ircuser );
  }
  members_.push_back ( std::move ( user ) );
}

void ProxySessionHandler::disconnectIrcSession ( const std::string &from ) {
  auto iter = ircSessions_.find ( from );
  if ( iter != ircSessions_.end ( ) ) {
    ircSessions_.erase ( iter );
  }
}

void ProxySessionHandler::run ( int timeout ) {
  auto users = db::select_users ( filename_ );
  logger::Log ( )->debug ( "Returns {} users", users.size ( ) );
  for ( const User &u : users ) {
    if ( u.listen ) {
      logger::Log ( )->info ( "Logging in {},{}", u.xmppuser, u.ircuser );
      addMember ( u, true );
    }
  }

  while ( !g_quit.load ( ) ) {
    if ( xmppsession_.shouldIConnect ( ) ) {
      xmppsession_.connect ( );
    }

    xmppsession_.recv ( timeout );
    //
    //
    //
    for ( auto it = ircSessions_.cbegin ( ), ite = ircSessions_.cend ( ); it != ite; ) {
      if ( !it->second.isConnected ( ) ) {
        it = ircSessions_.erase ( it );
      } else {
        it->second.recv ( timeout );
        ++it;
      }
    }

    logger::Log()->flush ( );

  }
}
