#include "log.hpp"

namespace logger {

namespace internal {
static LogShrdPtr spdlog = nullptr;
}
LogShrdPtr &Log ( ) { return internal::spdlog; }
// TODO: throw exception if log is not init
void init ( LOGTYPE logtype, std::string appname, std::string filename, int sizeInMeg, int max ) {
  // TODO: may need to break this up.. looking at the documentation,
  if ( LOGTYPE::CONSOLE_WRITER == logtype ) {
    internal::spdlog = spdlog::stdout_logger_mt ( appname );
  }
  if ( LOGTYPE::FILE_WRITER == logtype ) {
    internal::spdlog = spdlog::rotating_logger_mt ( appname, filename, 1024 * 1024 * sizeInMeg, max );
  }

  internal::spdlog->flush ( );
}
}  // end of namespace logger
