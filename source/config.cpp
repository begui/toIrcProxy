#include "config.hpp"
#include "dao.hpp"
#include "log.hpp"

Window::Window ( int rowOffset, int columnOffset ) {
  auto create_window = []( int lines, int columns, int y, int x ) { return newwin ( lines, columns, y, x ); };

  int screenLine, screenColumn;
  getmaxyx ( stdscr, screenLine, screenColumn );
  window_ = WindowUniqPtr ( create_window ( screenLine + rowOffset, screenColumn + columnOffset, 0, 0 ) );
}
void Window::highlight ( bool enable ) {
  enable ? ::wattron ( window_.get ( ), A_STANDOUT ) : ::wattroff ( window_.get ( ), A_STANDOUT );
}
void Window::refresh ( ) { wrefresh ( window_.get ( ) ); }
void Window::keypad ( bool enable ) {
  ::keypad ( window_.get ( ), enable );
  ::curs_set ( !enable );
}
void Window::write ( const char* str, int x, int y, bool bHighlight ) {
  highlight ( bHighlight );
  mvwprintw ( window_.get ( ), x, y, "%s", str );
}
void Window::clear ( ) { ::wclear ( window_.get ( ) ); }
void Window::clearLine ( int row, int column ) {
  wmove ( window_.get ( ), row, column );
  wclrtoeol ( window_.get ( ) );
}

void Window::input ( const char* label, std::string& input, int x, int y, bool hide ) {
  std::string stars (input.length(), '*');
  logger::Log()->debug("{}", stars);

  highlight ( false );
  auto print = [&]() {
    mvwprintw ( window_.get ( ), x, y, "%s", label );
    mvwprintw ( window_.get ( ), x, strlen ( label ) + y, "%s", hide ? stars.c_str() : input.c_str ( ) );
  };

  print();
  bool run{true};
  while ( run ) {
    int ch = getChar ( );
    switch ( ch ) {
      case KEY_ENTER:
      case 10:
        run = false;
        break;
      case KEY_BACKSPACE:
        int y, x;
        getyx ( window_.get ( ), y, x );  // save current pos
        wmove ( window_.get ( ), y , 1 );  // move to beginning of line + 1
        wclrtoeol ( window_.get ( ) );    // clear line
        wmove ( window_.get ( ), y, x );  // move to original position

        if ( !input.empty ( ) ) {
          input.pop_back ( );
        }
        if(!stars.empty()) {
          stars.pop_back ( );
        }
        break;
      case KEY_UP:
      case KEY_DOWN:
      case KEY_LEFT:
      case KEY_RIGHT:
        // DO nothing
        break;
      default:
        input.push_back ( ch );
        stars.push_back('*');
        break;
    }
    print();
  }
}

void Window::box ( std::uint32_t vert, std::uint32_t horz ) { ::box ( window_.get ( ), vert, horz ); }
int Window::getChar ( ) { return wgetch ( window_.get ( ) ); }

ConfigMenu::ConfigMenu ( ) { ::initscr ( ); }

ConfigMenu::~ConfigMenu ( ) { ::endwin ( ); }

void ConfigMenu::echo ( bool enable ) { enable ? ::echo ( ) : ::noecho ( ); }

ConfigMenu::Selection& operator++ ( ConfigMenu::Selection& selection ) {
  using IntType = typename std::underlying_type< ConfigMenu::Selection >::type;
  selection = static_cast< ConfigMenu::Selection > ( static_cast< IntType > ( selection ) + 1 );
  if ( selection == ConfigMenu::Selection::END ) {
    selection = static_cast< ConfigMenu::Selection > ( 1 );
  }
  return selection;
}

ConfigMenu::Selection& operator-- ( ConfigMenu::Selection& selection ) {
  using IntType = typename std::underlying_type< ConfigMenu::Selection >::type;
  selection = static_cast< ConfigMenu::Selection > ( static_cast< IntType > ( selection ) - 1 );
  if ( selection == ConfigMenu::Selection::BEGIN ) {
    selection = static_cast< ConfigMenu::Selection > ( static_cast< IntType > ( ConfigMenu::Selection::END ) - 1 );
  }
  return selection;
}

void ConfigMenu::next ( ) { ++currentMenuItem_; }
void ConfigMenu::prev ( ) { --currentMenuItem_; }

void ConfigMenu::run ( ) {
  Window menuWindow;
  menuWindow.box ( '|', '-' );

  menuWindow.write ( "Welcome to the toIrcProxy, please 'q' to quit", 1, 2, false );
  std::array< const char*, 3 > menuItem = {"irc", "xmpp", "path"};
  for ( std::size_t i = 0; i < menuItem.size ( ); ++i ) {
    menuWindow.write ( menuItem[ i ], i + 2, 2, i == 0 );
  }

  menuWindow.refresh ( );
  echo ( false );
  menuWindow.keypad ( true );

  auto configFun = [&]( ConfigMenu::Selection selection ) {
    constexpr int row{8};
    constexpr int column{5};
    const std::string dbName = Env::get ( ENV_VARS_TYPE::FILE_DB, default_dbname );
    std::string inputStr{""};

    auto upsertLookup = [&]( ENV_VARS_TYPE type, bool hide ) {
      inputStr = db::select_lookup_value ( dbName, Env::ENV_VARS[ type ].c_str ( ) );
      menuWindow.input ( ( Env::ENV_VARS[ type ] + ": " ).c_str ( ), inputStr, row, column, hide );
      db::insert_lookup ( dbName, Env::ENV_VARS[ type ], inputStr );
      menuWindow.clearLine ( row, 1 );
    };

    if ( ConfigMenu::Selection::Irc == selection ) {
      upsertLookup ( ENV_VARS_TYPE::JABBER_USER, false);
      upsertLookup ( ENV_VARS_TYPE::JABBER_PASS, true);
    }
    if ( ConfigMenu::Selection::Xmpp == selection ) {
      upsertLookup ( ENV_VARS_TYPE::IRC_HOST, false );
      upsertLookup ( ENV_VARS_TYPE::IRC_PORT, false);
    }
    if ( ConfigMenu::Selection::Path == selection ) {
      upsertLookup ( ENV_VARS_TYPE::FILE_DB, false);
      upsertLookup ( ENV_VARS_TYPE::FILE_LOG, false);
    }
  };

  int ch;
  while ( ( ch = menuWindow.getChar ( ) ) != 'q' ) {
    menuWindow.write ( "Welcome to the toIrcProxy, please 'q' to quit", 1, 2, false );
    switch ( ch ) {
      case KEY_UP:
        menuWindow.write ( menuItem[ ( getCurrentMenuItem ( ) - 1 ) ], ( getCurrentMenuItem ( ) - 1 ) + 2, 2, false );
        prev ( );
        menuWindow.write ( menuItem[ ( getCurrentMenuItem ( ) - 1 ) ], ( getCurrentMenuItem ( ) - 1 ) + 2, 2, true );
        break;
      case KEY_DOWN:
        menuWindow.write ( menuItem[ ( getCurrentMenuItem ( ) - 1 ) ], ( getCurrentMenuItem ( ) - 1 ) + 2, 2, false );
        next ( );
        menuWindow.write ( menuItem[ ( getCurrentMenuItem ( ) - 1 ) ], ( getCurrentMenuItem ( ) - 1 ) + 2, 2, true );
        break;
      case KEY_ENTER:
      case 10:
        configFun ( getCurrentMenuItem ( ) );
        break;
    }
  }
}
