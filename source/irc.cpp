#include "irc.hpp"
#include <algorithm>
#include <cstring>
#include <sstream>
#include "dao.hpp"
#include "log.hpp"

bool isNumeric ( const std::string &input ) { return std::all_of ( input.begin ( ), input.end ( ), ::isdigit ); }

static SessionPtr create_session ( irc_callbacks_t &callbacks ) {
  SessionPtr sessionPtr ( irc_create_session ( &callbacks ), []( irc_session_t *session ) {
    if ( session ) {
      irc_destroy_session ( session );
    }
  } );
  return sessionPtr;
}

static void handle_irc_message ( irc_ctx_t *ctx, IrcListener::IRC_EVENT event, const char *origin, const char **params,
                                 unsigned int count, unsigned int desiredIndex = 1 ) {
  logger::Log ( )->trace ( "handle_irc_message" );
  if ( ctx && ctx->listener ) {
    const char *param = ( count == desiredIndex + 1 ) ? params[ desiredIndex ] : "";
    logger::Log ( )->debug ( "origin: {}, param {} ", origin, param );
    logger::Log ( )->debug ( "nick: {} ", ctx->nick );
    ctx->listener->handleIrcMessage ( event, ctx->nick, origin, param );
  }
}

static void generic_event ( irc_session_t *session, const char *event, const char *origin, const char **params,
                            unsigned int count ) {
  logger::Log ( )->trace ( "generic_event" );
  std::stringstream ss;
  unsigned int cnt;
  for ( cnt = 0; cnt < count; cnt++ ) {
    if ( cnt ) {
      ss << "--";
    }
    ss << params[ cnt ];
  }

  logger::Log ( )->debug ( "DUMP --------------------------------" );
  logger::Log ( )->debug ( "Event: {}", event );
  logger::Log ( )->debug ( "Origin: {}", origin ? origin : "NULL" );
  logger::Log ( )->debug ( "Params Buf : {} - {}", cnt, ss.str ( ) );

  if ( isNumeric ( event ) ) {
    if ( std::atoi ( event ) == LIBIRC_RFC_RPL_NAMREPLY ) {
      irc_ctx_t *ctx = static_cast< irc_ctx_t * > ( irc_get_ctx ( session ) );
      handle_irc_message ( ctx, IrcListener::IRC_EVENT::NAMES, origin, params, count, 3 );
    }
  }
}

IrcSession::IrcSession ( const irc_ctx_t &ircCtx ) : ctx_{ircCtx} {
  std::memset ( &callbacks_, 0, sizeof ( callbacks_ ) );
  //
  // Required callback events
  //
  callbacks_.event_numeric = []( irc_session_t *session, unsigned int event, const char *origin, const char **params,
                                 unsigned int count ) {
    // Ignoring numeric events for now
    logger::Log ( )->trace ( "event_numeric" );
    char buf[ 24 ];
    sprintf ( buf, "%d", event );
    generic_event ( session, buf, origin, params, count );
  };
  //
  // Connecting
  //
  callbacks_.event_connect = []( irc_session_t *session, const char *event, const char *origin, const char **params,
                                 unsigned int count ) {
    logger::Log ( )->trace ( "event_connect" );
    if ( !irc_is_connected ( session ) ) {
      logger::Log ( )->error ( "Failed to connect to server" );
    }
    generic_event ( session, event, origin, params, count );

    irc_ctx_t *ctx = static_cast< irc_ctx_t * > ( irc_get_ctx ( session ) );
    if ( !ctx ) {
      logger::Log ( )->error ( "irc_ctx_t is null" );
    }
    if ( ctx->channel.c_str ( ) == nullptr ) {
      // Fixes crash since channel can be "null"
      return;
    }
    if ( ctx->passwd.c_str ( ) == nullptr ) {
      // fixes crash
      return;
    }

    logger::Log ( )->debug ( "Channel : {}", ctx->channel );
    logger::Log ( )->debug ( "Paswd : {}", ctx->passwd );
    if ( irc_cmd_join ( session, ctx->channel.c_str ( ), ctx->passwd.c_str ( ) ) ) {
      logger::Log ( )->error ( "Failed to join channel {}", ctx->channel );
    }
    handle_irc_message ( ctx, IrcListener::IRC_EVENT::CONNECT, origin, params, count );
  };
  //
  // Someone has joined the channel
  //
  callbacks_.event_join = []( irc_session_t *session, const char *event, const char *origin, const char **params,
                              unsigned int count ) {
    logger::Log ( )->trace ( "event_join" );
    generic_event ( session, event, origin, params, count );
    irc_cmd_user_mode ( session, "+i" );

    irc_ctx_t *ctx = static_cast< irc_ctx_t * > ( irc_get_ctx ( session ) );
    handle_irc_message ( ctx, IrcListener::IRC_EVENT::JOINED, origin, params, count );

  };
  //
  // Someone has quit the channel
  //
  callbacks_.event_quit = []( irc_session_t *session, const char *event, const char *origin, const char **params,
                              unsigned int count ) {
    logger::Log ( )->trace ( "event_quit" );
    generic_event ( session, event, origin, params, count );

    irc_ctx_t *ctx = static_cast< irc_ctx_t * > ( irc_get_ctx ( session ) );
    handle_irc_message ( ctx, IrcListener::IRC_EVENT::LEFT, origin, params, count );

  };
  //
  // Leaving
  //
  callbacks_.event_part = []( irc_session_t *session, const char *event, const char *origin, const char **params,
                              unsigned int count ) {
    logger::Log ( )->trace ( "event_part" );
    generic_event ( session, event, origin, params, count );

    irc_ctx_t *ctx = static_cast< irc_ctx_t * > ( irc_get_ctx ( session ) );
    handle_irc_message ( ctx, IrcListener::IRC_EVENT::LEFT, origin, params, count );
  };
  //
  // Channel message
  //
  callbacks_.event_channel = []( irc_session_t *session, const char *event, const char *origin, const char **params,
                                 unsigned int count ) {

    UNUSED ( event );
    logger::Log ( )->trace ( "event_channel" );

    if ( count != 2 ) {
      return;
    }
    if ( !origin ) {
      return;
    }

    irc_ctx_t *ctx = static_cast< irc_ctx_t * > ( irc_get_ctx ( session ) );
    handle_irc_message ( ctx, IrcListener::IRC_EVENT::CHAT, origin, params, count );
  };
  //
  // Private Message
  //
  callbacks_.event_privmsg = []( irc_session_t *session, const char *event, const char *origin, const char **params,
                                 unsigned int count ) {
    logger::Log ( )->trace ( "event_privmsg" );
    generic_event ( session, event, origin, params, count );
    irc_cmd_msg ( session, origin, "Private Messages are not supported!!!" );
  };

  callbacks_.event_nick = generic_event;
  callbacks_.event_mode = generic_event;
  callbacks_.event_topic = generic_event;
  callbacks_.event_kick = generic_event;
  callbacks_.event_notice = generic_event;
  callbacks_.event_invite = generic_event;
  callbacks_.event_umode = generic_event;
  callbacks_.event_ctcp_rep = generic_event;
  callbacks_.event_ctcp_action = generic_event;
  callbacks_.event_unknown = generic_event;

  session_ = create_session ( callbacks_ );
}

IrcSession::operator irc_session_t * ( ) const { return session_.get ( ); }

bool IrcSession::isConnected ( ) const { return 1 == irc_is_connected ( session_.get ( ) ); }

void IrcSession::getNames ( ) const {
  // relies on event_numeric, event 353
  irc_ctx_t *ctx = static_cast< irc_ctx_t * > ( irc_get_ctx ( session_.get ( ) ) );
  logger::Log ( )->debug ( "Getting names for {}", ctx->channel );
  irc_cmd_names ( session_.get ( ), ctx->channel.c_str ( ) );
}

void IrcSession::connect ( ) {
  if ( session_ ) {
    // We bind the ctx data to this so we can retrieve it later
    irc_set_ctx ( session_.get ( ), &ctx_ );

    // Enable automatic parsing of nicknames
    irc_option_set ( session_.get ( ), LIBIRC_OPTION_STRIPNICKS );

    int rtn = irc_connect ( session_.get ( ), dao::getIrcHost ( ).c_str ( ), dao::getIrcPort ( ), 0,
                            ctx_.nick.c_str ( ), nullptr, nullptr );
    if ( rtn ) {
      logger::Log ( )->error ( "Failed to connect with error code {}", rtn );
    }
    if ( !isConnected ( ) ) {
      logger::Log ( )->error ( "is not connected {}", rtn );
    }
  }
}

void IrcSession::send ( const std::string &msg ) const {
  irc_ctx_t *ctx = static_cast< irc_ctx_t * > ( irc_get_ctx ( session_.get ( ) ) );
  irc_cmd_msg ( session_.get ( ), ctx->channel.c_str ( ), msg.c_str ( ) );
}

void IrcSession::recv ( int microseconds ) const {
  if ( !isConnected ( ) ) {
    logger::Log ( )->error ( " Session is no longer connected." );
    return;
  }
  int maxfd = 0;
  fd_set readset, writeset;
  FD_ZERO ( &readset );
  FD_ZERO ( &writeset );
  if ( irc_add_select_descriptors ( session_.get ( ), &readset, &writeset, &maxfd ) < 0 ) {
    logger::Log ( )->error ( "Failed to add selector to descriptor. Did you call connect?" );
    logger::Log ( )->error ( "Error {}", irc_strerror ( irc_errno ( session_.get ( ) ) ) );
    return;
  }
  struct timeval tv;
  tv.tv_sec = 0;
  tv.tv_usec = microseconds;
  int result = ::select ( maxfd + 1, &readset, &writeset, nullptr, &tv );
  if ( result >= 0 ) {
    if ( result == 0 ) {
      // This is a timeout
    } else {
      if ( irc_process_select_descriptors ( session_.get ( ), &readset, &writeset ) ) {
        logger::Log ( )->error ( "The connection Failed or the server disconnected. " );
      }
    }
  }
}
