#include "dao.hpp"
#include <algorithm>

#include "log.hpp"

std::map< ENV_VARS_TYPE, std::string > Env::ENV_VARS = {

    {ENV_VARS_TYPE::FILE_DB, "FILE_DB"},
    {ENV_VARS_TYPE::FILE_LOG, "FILE_LOG"},
    {ENV_VARS_TYPE::JABBER_USER, "JABBER_USER"},
    {ENV_VARS_TYPE::JABBER_PASS, "JABBER_PASS"},
    {ENV_VARS_TYPE::IRC_HOST, "IRC_HOST"},
    {ENV_VARS_TYPE::IRC_PORT, "IRC_PORT"},
    {ENV_VARS_TYPE::POLL_INTERVAL, "POLL_INTERVAL"}};

const char *Env::get ( ENV_VARS_TYPE type ) noexcept { return std::getenv ( ENV_VARS[ type ].c_str ( ) ); }
const char *Env::get ( ENV_VARS_TYPE type, const char *defaultVal ) noexcept {
  auto str = Env::get ( type );
  return ( str ) ? str : defaultVal;
}
const std::string Env::get ( ENV_VARS_TYPE type, const std::string &defaultVal ) noexcept {
  auto str = Env::get ( type );
  return ( str ) ? str : defaultVal;
}

std::int32_t Env::get ( ENV_VARS_TYPE type, std::int32_t defaultVal ) noexcept {
  std::string val = get ( type );
  bool isNotDigit =
      std::find_if ( val.begin ( ), val.end ( ), []( char c ) { return !std::isdigit ( c ); } ) == val.end ( );
  if ( !isNotDigit ) {
    return defaultVal;
  }
  return std::stoi ( val );
}

bool Env::exist ( ENV_VARS_TYPE type ) noexcept { return Env::get ( type ) != nullptr; }
namespace db {

void init ( const std::string &dbname ) {
  sqlite::database db ( dbname );

  db << "CREATE TABLE IF NOT EXISTS user_role ("
        "_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
        "name TEXT UNIQUE NOT NULL,"
        "UNIQUE(name)"
        ");";

  db << "INSERT OR IGNORE INTO user_role (name ) VALUES ( 'ADMIN' );";
  db << "INSERT OR IGNORE INTO user_role (name) VALUES ( 'USER' );";

  db << "CREATE TABLE IF NOT EXISTS user ("
        "_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
        "role INTEGER,"
        "xmppuser TEXT UNIQUE NOT NULL,"
        "listen NUMERIC DEFAULT 0,"
        "ircuser TEXT,"
        "ircpass TEXT,"  // TODO: encrypt this
        "FOREIGN KEY(role) REFERENCES user_role(_id)"
        ");";

  db << "CREATE TABLE IF NOT EXISTS lookup ("
        "_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
        "name TEXT UNIQUE NOT NULL,"
        "val TEXT NOT NULL,"
        "UNIQUE(name)"
        ");";
}

void insert_lookup ( const std::string &dbname, const std::string &name, const std::string &value ) {
  sqlite::database db ( dbname );
  if ( name.length ( ) == 0 ) {
    return;
  }
  db << "INSERT OR REPLACE INTO lookup (name, val) values ( ?, ?);" << name << value;
}

std::string select_lookup_value ( const std::string &dbname, const std::string &name ) {
  std::string val{""};
  if ( check_lookup ( dbname, name ) ) {
    sqlite::database db ( dbname );
    db << "SELECT val FROM lookup WHERE name = ?" << name >> val;
  }
  return val;
}
bool check_lookup ( const std::string &dbname, const std::string &name ) {
  int count{0};
  sqlite::database db ( dbname );
  db << "SELECT COUNT(*) FROM lookup WHERE name = ?" << name >> count;
  return ( 0 != count );
}

bool check_irc_user ( const std::string &dbname, const std::string &ircuser ) {
  int count{0};
  sqlite::database db ( dbname );
  db << "SELECT COUNT(*) FROM user WHERE ircuser = ?" << ircuser >> count;
  return ( 0 != count );
}

std::string getXmppUser ( const std::string &dbname, const std::string &ircuser ) {
  std::string xmppuser;
  sqlite::database db ( dbname );
  db << "SELECT xmppuser FROM user WHERE ircuser = ? " << ircuser >> xmppuser;
  return xmppuser;
}

void insert_irc_user ( const std::string &dbname, const std::string &xmppuser, const std::string &ircuser,
                       const std::string &ircpass ) {
  sqlite::database db ( dbname );
  if ( !check_irc_user ( dbname, ircuser ) ||
       ( check_irc_user ( dbname, ircuser ) && xmppuser.compare ( getXmppUser ( dbname, ircuser ) ) == 0 ) ) {
    auto roleValue = static_cast< std::underlying_type< User::USER_ROLE >::type > ( User::USER_ROLE::USER );
    db << "INSERT OR REPLACE INTO user (role, xmppuser, ircuser, ircpass, listen) values ( ?, ?, ?, ?, ?);" << roleValue
       << xmppuser << ircuser << ircpass << 1;
  } else {
    logger::Log ( )->warn ( "Failed to insert {} to {} ", ircuser, xmppuser );
  }
}

void update_listen ( const std::string &dbname, const std::string &ircuser, bool listen ) {
  sqlite::database db ( dbname );
  db << "UPDATE USER SET listen = ? WHERE xmppuser = ? ;" << int( listen ) << ircuser;
}

std::vector< User > select_users ( const std::string &dbname ) {
  std::vector< User > users;

  sqlite::database db ( dbname );
  db << "SELECT xmppuser, ircuser,ircpass, role, listen  FROM user" >>
      [&]( std::string xmppuser, std::string ircuser, std::unique_ptr< std::string > ircpass, int role, int listen ) {

        users.emplace_back ( xmppuser, ircuser, ( !ircpass ) ? *ircpass : "", listen,
                             ( role == 1 ) ? User::USER_ROLE::ADMIN : User::USER_ROLE::USER );
      };

  return users;
}

std::unique_ptr< User > select_user ( const std::string &dbname, const std::string &xmppuser ) {
  std::unique_ptr< User > user = nullptr;
  sqlite::database db ( dbname );
  db << "SELECT xmppuser, ircuser,ircpass, role, listen  FROM user where xmppuser = ? " << xmppuser >>
      [&]( std::string xmppuser, std::string ircuser, std::unique_ptr< std::string > ircpass, int role, int listen ) {

        user.reset ( new User ( xmppuser, ircuser, ( !ircpass ) ? *ircpass : "", listen,
                                ( role == 1 ) ? User::USER_ROLE::ADMIN : User::USER_ROLE::USER ) );
      };

  return user;
}
}  // end of namespace db

namespace validation {
bool check ( ENV_VARS_TYPE type ) {
  return Env::exist ( type ) || ( db::select_lookup_value (                                 //
                                      Env::get ( ENV_VARS_TYPE::FILE_DB, default_dbname ),  //
                                      Env::ENV_VARS[ type ].c_str ( )                       //
                                      ) != "" );
}
}  // end of namespace validation

namespace dao {

std::string getJabberUser ( ) {
  ENV_VARS_TYPE type = ENV_VARS_TYPE::JABBER_USER;
  const std::string dbName = Env::get ( ENV_VARS_TYPE::FILE_DB, default_dbname );
  auto val = db::select_lookup_value ( dbName,
                                       Env::ENV_VARS[ type ].c_str ( )  //
                                       );
  return Env::get ( type, val );
}
std::string getJabberPass ( ) {
  ENV_VARS_TYPE type = ENV_VARS_TYPE::JABBER_PASS;
  const std::string dbName = Env::get ( ENV_VARS_TYPE::FILE_DB, default_dbname );
  auto val = db::select_lookup_value ( dbName,
                                       Env::ENV_VARS[ type ].c_str ( )  //
                                       );
  return Env::get ( type, val );
}
std::string getIrcHost ( ) {
  ENV_VARS_TYPE type = ENV_VARS_TYPE::IRC_HOST;
  const std::string dbName = Env::get ( ENV_VARS_TYPE::FILE_DB, default_dbname );
  auto val = db::select_lookup_value ( dbName,
                                       Env::ENV_VARS[ type ].c_str ( )  //
                                       );
  return Env::get ( type, val );
}
std::int32_t getIrcPort ( ) {
  ENV_VARS_TYPE type = ENV_VARS_TYPE::IRC_PORT;
  const std::string dbName = Env::get ( ENV_VARS_TYPE::FILE_DB, default_dbname );
  auto val = db::select_lookup_value ( dbName,
                                       Env::ENV_VARS[ type ].c_str ( )  //
                                       );
  if(val == "") {
    val = "8001";
  }
  return Env::get ( type, std::stoi ( val ) );
}

}  // end of namespace dao
